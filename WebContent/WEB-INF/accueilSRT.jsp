<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<title>accueilSRT</title>
	</head>
	
	<body>
		
		<div class="container">
			<header class="page-header">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h1> Bienvenue ! </br></h1>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6">
						<p>Suivez ce lien pour upload un fichier à traduire</br></p>
						<a href="/OC_activite_JAVA2E_editeur_srt/uploadSRT">Upload</a>
					</div>

					<div class="col-lg-6">
						<p>
					    	Vous pouvez telecharger le fichier .srt avec sa traduction.</br></p>
						    <form method="post" action="downloadSRT">
						    	<p>
							    	<label for="fichierdl">Fichier à telecharger : </label>
							    	<select name="fichierdl" id="fichierdl">
							    		<c:forEach items="${fichierBDD}" var="fichier">
							    			<option id="fichierDDL" value="${fichier}"><c:out value="${fichier}"></c:out></option>
							    		</c:forEach>
							    	</select>
						    	</p>
						    	<input type="submit" value="GENERER LE LIEN"/>
						    	<c:if test="${ !empty nomFichierdl }">
						    		
						    		<p><a href="C:/Users/nicolas/Documents/tmp/${nomFichierdl }"><c:out value="votre lien "></c:out></a></p>
						    	</c:if>
						    </form>
					   
					</div>  	
				</div>
			</header>
		
			<h2>Fichier en cours de traduction : ${nomFichier}</h2>
			<form method="post">    
		        <input type="submit" value="TRADUIRE" style="position:fixed; top: 10px; right: 10px;" />
			    <table class="table table-bordered table-striped">
			        <c:forEach items="${sousTitre}" var="line" varStatus="status">
			        	<tr>
			        		<td style="text-align:right;"><c:out value="${ line }" /></td>
			        		<td><input type="text" name="line${ status.index }" id="line${ status.index }" size="35" value="${trad.get(status.index)}" /></td>
			        	</tr>
			    	</c:forEach>
			    </table>
	    	</form>
		</div>	
	</body>
</html>