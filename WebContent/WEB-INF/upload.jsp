<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<title>Upload</title>
	</head>
	
	<body>
		<h2>Upload de fichiers</h2>
		
		<div>
			<p> 
				Vous pouvez déposer le fichier .srt ci-dessous afin de le traduire. </br>
				Par défaut, le fichier à traduire sera "password_presentation.srt".
			</p>
								
			<c:if test="${erreur }"><p><c:out value="Le fichier ${ fichier } (${ nomFichier }) existe deja dans la base !" /></p></c:if>		
			
			<form method="post" action="uploadSRT" enctype="multipart/form-data">
			    <p>
			        <label for="nomFichier">nom du fichier : </label>
			        <input type="text" name="nomFichier" id="nomFichier" />
			   	</p>
			    <p>
			        <label for="langue_native">Langue native : </label>
			        <input type="text" name="langue_native" id="langue_native" />
			   	</p>
			   	<p>
			        <label for="langue_trad">Langue de traduction : </label>
			        <input type="text" name="langue_trad" id="langue_trad" />
			   	</p>
			    <p>
			         <label for="fichier">Fichier à envoyer : </label>
			         <input type="file" name="fichier" id="fichier" />
			    </p>
			    <input type="submit" value="UPLOAD"/>
			</form>
			<c:if test="${ !empty fichier }"><p><c:out value="Le fichier ${ fichier } (${ nomFichier }) a été uploadé et est prêt à être traduit !" /></p></c:if>
			<a href="/OC_activite_JAVA2E_editeur_srt/editSRT?${nomFichier}">retour à l'accueil pour la traduction du fichier ${nomFichier}</a>	
					
		</div>
	</body>
</html>