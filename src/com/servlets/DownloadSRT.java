package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gestion.fichier.FileHandler;


/**
 * Servlet implementation class DownloadSRT
 */
@WebServlet("/DownloadSRT")
public class DownloadSRT extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadSRT() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println("TRACE DOGET");//--Controle
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueilSRT.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String cheminFichier = request.getParameter("fichierdl");
		System.out.println("chemin DDL: "+cheminFichier);
		FileHandler fh = new FileHandler(cheminFichier);
		fh.fichierTraduit();
		request.setAttribute("nomFichierdl", cheminFichier);
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueilSRT.jsp").forward(request, response);
	}

}
