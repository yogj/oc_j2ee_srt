package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.beans.FichierSRT;
import com.beans.SousTitre;
import com.gestion.fichier.AffichageHandler;
import com.gestion.fichier.FileHandler;
import com.patternDAO.DAOAbstract;
import com.patternDAO.FichierSRT_DAO;
import com.patternDAO.MySQLConnection;
import com.patternDAO.SousTitre_DAO;

/**
 * Servlet implementation class UploadSRT
 */
@WebServlet("/UploadSRT")
public class UploadSRT extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final int TAILLE_TAMPON = 10240;
    public static final String CHEMIN_FICHIERS = "C:\\Users\\nicolas\\Documents\\tmp\\";
    private FileHandler fh = new FileHandler(CHEMIN_FICHIERS);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadSRT() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		this.getServletContext().getRequestDispatcher("/WEB-INF/upload.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// On r�cup�re le champ description comme d'habitude
        String nomFich = request.getParameter("nomFichier");
        request.setAttribute("nomFichier", nomFich );
		String langueNative = request.getParameter("langue_native");
        request.setAttribute("langue_native", langueNative );
        String langueTrad = request.getParameter("langue_trad");
        request.setAttribute("langue_trad", langueTrad );
        // On r�cup�re le champ du fichier
        Part part = request.getPart("fichier");

        // On v�rifie qu'on a bien re�u un fichier
        String nomFichier = FileHandler.getNomFichier(part);

        // Si on a bien un fichier
        if (nomFichier != null && !nomFichier.isEmpty()) {
            String nomChamp = part.getName();
            // Corrige un bug du fonctionnement d'Internet Explorer
            nomFichier = nomFichier.substring(nomFichier.lastIndexOf('/') + 1).substring(nomFichier.lastIndexOf('\\') + 1);

            // On �crit d�finitivement le fichier sur le disque
            fh.ecrireFichier(part, nomFichier, CHEMIN_FICHIERS); 
  
            //--on l'ecrit dans la base de donnee
            DAOAbstract<FichierSRT> fileDAO = new FichierSRT_DAO(MySQLConnection.getInstance());
            if (! (fileDAO.find(nomFich).getId()>0)) {
            	FichierSRT fichier = new FichierSRT(nomFich);
              	fichier.setLangueOrig(langueNative);
            	fichier.setLangueTrad(langueTrad);
            	fichier.setAvancement(0);
            	fileDAO.create(fichier);
            	
                //--on lit le fichier pour recuperer les sous titres originaux            	
                SousTitre subtitle = fh.lireFichierOrig(CHEMIN_FICHIERS+nomFichier);
                //--on recupere l'id du fichier dans la bdd
                subtitle.setIdFichier(fileDAO.find(nomFich).getId());
                
              //--je remplis la liste de sous titre trad
    			subtitle.setSousTitreTrad(AffichageHandler.getSSTitreTradHandler(subtitle.getSousTitreOrig()));
               
    			DAOAbstract<SousTitre> ssTitreDAO = new SousTitre_DAO(MySQLConnection.getInstance());
                ssTitreDAO.create(subtitle);
             }
 
      }
        System.out.println("param "+nomFich);
        request.setAttribute("nomFichier", nomFich);
        this.getServletContext().getRequestDispatcher("/WEB-INF/upload.jsp").forward(request, response);
	}
        

}
