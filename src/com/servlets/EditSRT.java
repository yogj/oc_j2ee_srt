package com.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beans.FichierSRT;
import com.beans.SousTitre;
import com.gestion.fichier.AffichageHandler;
import com.patternDAO.DAOAbstract;
import com.patternDAO.FichierSRT_DAO;
import com.patternDAO.MySQLConnection;
import com.patternDAO.SousTitre_DAO;

/**
 * Servlet implementation class EditSRT
 */
@WebServlet("/EditSRT")
public class EditSRT extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String FILE_PATH = "/WEB-INF/sous_titres/password_presentation.srt";
	private static final String FILE_NAME = "password_presentation.srt";
	private String nomFile;
	private ArrayList<String> sousTitreOrigInter = new ArrayList<String>();
	private ArrayList<String> sousTitreTradInter = new ArrayList<String>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditSRT() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		DAOAbstract<FichierSRT> fileSRTDAO = new FichierSRT_DAO(MySQLConnection.getInstance());
		DAOAbstract<SousTitre> ssTTDAO = new SousTitre_DAO(MySQLConnection.getInstance());
		System.out.println(fileSRTDAO.find(FILE_NAME).getId());//--Controle
		
		//String nomFile = request.getParameter("nomFichier");
		nomFile = request.getQueryString();
		System.out.println(nomFile);//--Controle
		
		//--je verifie la presence du fichier ds la bdd, s'il y est, je r�cup�re les donn�es de la bdd    s'il n'y est pas j'ecris les donnees ds la bdd
		if (nomFile != null && fileSRTDAO.find(nomFile).getId()>0) {
			//--je recupere les sous titres
			sousTitreOrigInter = fileSRTDAO.find(nomFile).getSsTitre().getSousTitreOrig();
			sousTitreTradInter = fileSRTDAO.find(nomFile).getSsTitre().getSousTitreTrad();
			//--Controle
			System.out.println("taille inter : "+ sousTitreOrigInter.size() +" - "+AffichageHandler.constrAff(sousTitreOrigInter).size());
			System.out.println("taille inter : "+ sousTitreTradInter.size()+" - "+ AffichageHandler.constrAff(sousTitreTradInter).size());
			
			request.setAttribute("nomFichier", nomFile);
		}
		//--S'il n'y est pas on v�rifie la pr�sence du fichier par defaut ds la bdd et on �crit les donn�es ds la bdd le cas echeant
		else {
			if (! (fileSRTDAO.find(FILE_NAME).getId()>0)) {
				//--on r�cup�re les sous titres dans un objet SousTitre
				ServletContext contextG = getServletContext();
				System.out.println(contextG.getRealPath(FILE_PATH));
				SousTitre subTTHandler = new SousTitre(contextG.getRealPath(FILE_PATH));
				ArrayList<String> ssTTTrad = new ArrayList<String>();
				//--on cr�e un objet fichierSRT qui heberge les donn�es a stocker dans la table fichier_sous_titre
				FichierSRT fileSubTitle = new FichierSRT(FILE_NAME);
				fileSubTitle.setLangueOrig("francais");
				fileSubTitle.setLangueTrad("francais");
				fileSubTitle.setAvancement(0);
				//--on ecrit dans la table fichier_sous_titre
				fileSRTDAO.create(fileSubTitle);
				//--on definit les proprietes de l'objet SousTitre
				subTTHandler.setIdFichier(fileSRTDAO.find(FILE_NAME).getId());
				//--je remplis la liste de sous titre trad
				for (String ligne : subTTHandler.getSousTitreOrig()) {
					if(! AffichageHandler.controleAff(ligne))
						ssTTTrad.add(ligne);
					else
						ssTTTrad.add("");
				}
				subTTHandler.setSousTitreTrad(ssTTTrad);
				//--on ecrit les sous titres dans la table sous_titre
				ssTTDAO.create(subTTHandler);
			}
			request.setAttribute("nomFichier", FILE_NAME);
			//--je recupere les sous titres
			sousTitreOrigInter = fileSRTDAO.find(FILE_NAME).getSsTitre().getSousTitreOrig();
			sousTitreTradInter = fileSRTDAO.find(FILE_NAME).getSsTitre().getSousTitreTrad();
			//--Controle
			System.out.println("taille inter : "+ sousTitreOrigInter.size() +" - "+AffichageHandler.constrAff(sousTitreOrigInter).size());
			System.out.println("taille inter : "+ sousTitreTradInter.size()+" - "+ AffichageHandler.constrAff(sousTitreTradInter).size());
		}
		
		//--je les affiche
		request.setAttribute("sousTitre", AffichageHandler.constrAff(sousTitreOrigInter));		
		
		ArrayList<String> ssTTAff = new ArrayList<String>();
		for (int i = 0; i<sousTitreOrigInter.size(); i++) {
			if (AffichageHandler.controleAff(sousTitreOrigInter.get(i)) && sousTitreTradInter.get(i).isEmpty())
				ssTTAff.add("");
			else if (AffichageHandler.controleAff(sousTitreOrigInter.get(i)))
					ssTTAff.add(sousTitreTradInter.get(i));				
		}
		request.setAttribute("trad", ssTTAff);
		
		//--on recupere la liste des fichiers presents ds la bdd pour les afficher ds liste deroulante pour le telechargement
		request.setAttribute("fichierBDD", AffichageHandler.getNomFichBdd());

		//--on envoie les infos � la JSP
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueilSRT.jsp").forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SousTitre tradssTitre = new SousTitre();
		DAOAbstract<FichierSRT> fileSRTDAO = new FichierSRT_DAO(MySQLConnection.getInstance());
		//--on recupere le nom du fichier a traduire
		if (request.getQueryString() != null)
			nomFile = request.getQueryString();
		else
			nomFile = FILE_NAME;
		request.setAttribute("nomFichier", nomFile);
		System.out.println("fichier "+nomFile+" id "+fileSRTDAO.find(nomFile).getId());//--Controle
		//--je recupere l'id du fichier a traduire
		int id = fileSRTDAO.find(nomFile).getId();
		
		//int id = fileSRTDAO.find(FILE_NAME).getId();
		tradssTitre.setIdFichier(id);
		//--je recupere dans la bdd les listes de sous titres
		DAOAbstract<SousTitre> tradSousTitreDAO = new SousTitre_DAO(MySQLConnection.getInstance());
		sousTitreOrigInter = tradSousTitreDAO.find(id).getSousTitreOrig();
		sousTitreTradInter = tradSousTitreDAO.find(id).getSousTitreTrad();
		
		ArrayList<String>sstttrad = new ArrayList<String>();
		int j =0;
		//--je remplace les sous titres trad 
		
		for (String ligne : sousTitreOrigInter) {
			if (AffichageHandler.controleAff(ligne)) {
				sstttrad.add(request.getParameter("line"+j));
				j++;
			}
			else
				sstttrad.add(ligne);
		}
		
		

		//--je d�finis les attributs de l'objet a ecrire en bdd
		tradssTitre.setSousTitreOrig(sousTitreOrigInter);
		tradssTitre.setSousTitreTrad(sstttrad);
		//--j'ecris ds la bdd
		tradSousTitreDAO.update(tradssTitre);
		//--on affiche les donnees dans la jsp
		request.setAttribute("sousTitre", AffichageHandler.constrAff(tradSousTitreDAO.find(id).getSousTitreOrig()));

		ArrayList<String> ssTTAff = new ArrayList<String>();
		sousTitreTradInter = tradSousTitreDAO.find(id).getSousTitreTrad();
		for (int i = 0; i<sousTitreOrigInter.size(); i++) {
			if (AffichageHandler.controleAff(sousTitreOrigInter.get(i)) && sousTitreTradInter.get(i).isEmpty())
				ssTTAff.add("");
			else if (AffichageHandler.controleAff(sousTitreOrigInter.get(i)))
					ssTTAff.add(sousTitreTradInter.get(i));				
		}
		request.setAttribute("trad", ssTTAff);
	
		//--on recupere la liste des fichiers presents ds la bdd pour les afficher ds liste deroulante pour le telechargement
		request.setAttribute("fichierBDD", AffichageHandler.getNomFichBdd());
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueilSRT.jsp").forward(request, response);
	}
}
