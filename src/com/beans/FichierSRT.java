package com.beans;


/**
 * Objet fichier h�berge les donn�es de la bdd ou d'un fichier .srt
 * @author nicolas
 *
 */
public class FichierSRT {


	private static final long serialVersionUID = 1L;
	private int id, avancement;
	private String nom, langueOrig, langueTrad;
	private SousTitre ssTitre;


	public FichierSRT() {}
	
	/**
	 * Constructeur avec parametre
	 * @param pNom
	 */
	public FichierSRT(String pNom) {
		this.nom = pNom;
	}	
	public SousTitre getSsTitre() {
		return ssTitre;
	}
	public void setSsTitre(SousTitre ssTitre) {
		this.ssTitre = ssTitre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAvancement() {
		return avancement;
	}
	public void setAvancement(int avancement) {
		this.avancement = avancement;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getLangueOrig() {
		return langueOrig;
	}
	public void setLangueOrig(String langueOrig) {
		this.langueOrig = langueOrig;
	}
	public String getLangueTrad() {
		return langueTrad;
	}
	public void setLangueTrad(String langueTrad) {
		this.langueTrad = langueTrad;
	}
	
	
	
}
