package com.beans;

import java.util.ArrayList;

import com.gestion.fichier.FileHandler;

/**
 * Objet SousTitre h�berge les donn�es de la bdd ou les sous titres d'un fichier .srt
 * @author nicolas
 *
 */

public class SousTitre {
	
	private ArrayList<String> sousTitreOrig = null;
	private ArrayList<String> sousTitreTrad = null;
	private int id_fichier;
	private int ligne;
	
	/**
	 * Constructeur sans parametres
	 */
	public SousTitre() {
		this.sousTitreOrig = new ArrayList<String>();
		this.sousTitreTrad = new ArrayList<String>();
		this.id_fichier = 0;
		this.ligne = 0;
	}
	
	
	/**
	 * Constructeur avec parametre
	 * @param pNomFichier
	 */
	public SousTitre(String pNomFichier) {

		FileHandler fh = new FileHandler (pNomFichier);
		
		this.sousTitreOrig = fh.lireFichierOrig(pNomFichier).getSousTitreOrig();
		this.sousTitreTrad = fh.lireFichierOrig(pNomFichier).getSousTitreTrad();
	}
	
	public ArrayList<String> getSousTitreOrig() {
		return sousTitreOrig;
	}
	public void setSousTitreOrig(ArrayList<String> sousTitreOrig) {
		this.sousTitreOrig = sousTitreOrig;
	}
	public ArrayList<String> getSousTitreTrad() {
		return sousTitreTrad;
	}
	public void setSousTitreTrad(ArrayList<String> sousTitreTrad) {
		this.sousTitreTrad = sousTitreTrad;
	}
	
	public void setIdFichier(int pId) {
		this.id_fichier = pId;
	}
	
	public int getIdFichier() {
		return this.id_fichier;
	}
	
	public int getLigne() {
		return this.ligne;
	}


	public void setLigne(int ligne) {
		this.ligne = ligne;
	}
	
	
}
