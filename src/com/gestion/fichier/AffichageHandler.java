package com.gestion.fichier;

import java.util.ArrayList;

import com.beans.FichierSRT;
import com.patternDAO.DAOAbstract;
import com.patternDAO.FichierSRT_DAO;
import com.patternDAO.MySQLConnection;

public final class AffichageHandler {

	
	
	/**
	 * Methode pour recuperer les noms des fichiers presents en bdd pour l'affichage
	 * 
	 */
	public static ArrayList<String> getNomFichBdd(){
		DAOAbstract<FichierSRT> fileSRTDAO = new FichierSRT_DAO(MySQLConnection.getInstance());
		ArrayList<String> nomFichBdd = fileSRTDAO.listerFichier();

		return nomFichBdd;
	}
	
	/**
	 * Methode pour recuperer une liste de sous titre a afficher
	 * @param plistSousTitre
	 * @return
	 */
	public static ArrayList<String> getSSTitreTradHandler(ArrayList<String> plistSousTitre){
		ArrayList<String>ssTTTrad = new ArrayList<String>();
		for (String ligne : plistSousTitre) {
			if(! AffichageHandler.controleAff(ligne))
				ssTTTrad.add(ligne);
			else
				ssTTTrad.add("");
		}
		return ssTTTrad;		
	}
	
		
	/**
	 * M�thode trier les textes
	 * @param pTexte
	 * @return check
	 */
	
	public static boolean controleAff(String pTexte) {
		char[] filtre = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
		boolean check = false;
		//System.out.println(pTexte);
		if ((pTexte == null) || pTexte.isEmpty())
			check = false;
		else {
			for (int i =0; i<filtre.length; i++) {
				if (pTexte.charAt(0) != filtre[i]) {
					//System.out.println(pTexte.charAt(0));
					check =true;
				}
				else {
					check = false;
					break;
				}
			}
		}
		//System.out.println(check);
		return check;
	}
	
	/**
	 * Methode qui renvoie les textes a afficher
	 * @param pSousTitre
	 * @return sousTTAff
	 */
	public static ArrayList<String> constrAff(ArrayList<String> pSousTitre){
		ArrayList<String> sousTTAff = new ArrayList<String>();
		for (String ligne : pSousTitre) {
			if (controleAff(ligne)) {
				sousTTAff.add(ligne);
			}
		}
		
		return sousTTAff;	
		
	}
}
