package com.gestion.fichier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.Part;

import com.beans.FichierSRT;
import com.beans.SousTitre;
import com.patternDAO.DAOAbstract;
import com.patternDAO.FichierSRT_DAO;
import com.patternDAO.MySQLConnection;

/**
 * Classe de gestion des flux avec les fichiers
 * @author nicolas
 *
 */
public class FileHandler {

	private static final int TAILLE_TAMPON = 10240;
    private FichierSRT fichierSSTitre;
    private File fileTrad;
    private SousTitre sousTitre;
    private ArrayList<String> sousTitreOrig = null;
	private ArrayList<String> sousTitreTrad = null;
    
	/**
	 * Constructeur avec parametre
	 * @param pFile_Name
	 */
    public FileHandler(String pFile_Name) {
    	this.sousTitre = new SousTitre();
    	
    	this.fichierSSTitre = new FichierSRT(pFile_Name);
    }
    
    /**
     * Methode pour recuperer le cheminet le nom du fichier
     * @param part
     * @return
     */
    public static String getNomFichier( Part part ) {
        for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
            if ( contentDisposition.trim().startsWith( "filename" ) ) {
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        return null;
    }
    
    /**
     * M�thode pour r�cup�rer les sous titres d'un fichier
     * @param pFile_name
     * @return
     */
    public SousTitre lireFichierOrig(String pFile_name) {
    	sousTitreOrig = new ArrayList<String>();
    	sousTitreTrad = new ArrayList<String>();
	   BufferedReader br;
	   try {
	   		br = new BufferedReader(new FileReader(pFile_name));
	   		String line;
	   		while((line = br.readLine()) != null) {
	   			String lineUTF8 = new String(line.getBytes(),"UTF-8");
	   			sousTitreOrig.add(lineUTF8);
	   			sousTitreTrad.add("");
	   		}
	   		br.close();
	   }catch(IOException e) {
	   		e.printStackTrace();
	   }
	   sousTitre.setSousTitreOrig(sousTitreOrig);	   
	   sousTitre.setSousTitreTrad(sousTitreTrad);
	   return sousTitre;
    }
    
    public FichierSRT getFichierSSTitre() {
		return fichierSSTitre;
	}

	public void setFichierSSTitre(FichierSRT fichierSSTitre) {
		this.fichierSSTitre = fichierSSTitre;
	}

	/**
	 * M�thode pour upload un fichier
	 * @param part
	 * @param nomFichier
	 * @param chemin
	 * @throws IOException
	 */
	public void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            entree = new BufferedInputStream(part.getInputStream(), TAILLE_TAMPON);
            sortie = new BufferedOutputStream(new FileOutputStream(new File(chemin + nomFichier)), TAILLE_TAMPON);

            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur;
            while ((longueur = entree.read(tampon)) > 0) {
                sortie.write(tampon, 0, longueur);
            }
        } finally {
            try {
                sortie.close();
            } catch (IOException ignore) {

            }
            try {
                entree.close();
            } catch (IOException ignore) {

            }
        }
    }
	
	/**
	 * M�thode pour cr�er un fichier � ddl
	 */
	public void fichierTraduit() {
		//--creation du fichier vide
		fileTrad= new File("C:\\Users\\nicolas\\Documents\\tmp\\"+fichierSSTitre.getNom());
		System.out.println(fichierSSTitre.getNom());
		
		try {
            fileTrad.createNewFile();
			}catch (IOException e) {
            e.printStackTrace();
		}
		//--on recupere les donnees a ecrire dans la bdd
		DAOAbstract<FichierSRT> fsrtDAO = new FichierSRT_DAO(MySQLConnection.getInstance());
		fichierSSTitre = fsrtDAO.find(fichierSSTitre.getNom());
		
		
		//--on ecrit dans le fichier
		try {
			BufferedWriter fichSRT = new BufferedWriter(new FileWriter(fileTrad));
			
			for (String s : fichierSSTitre.getSsTitre().getSousTitreTrad()) {
				fichSRT.write(s);
				fichSRT.newLine();
			}
			
			fichSRT.close();
			System.out.println("Taille du fichier = "+fileTrad.getName()+" " + fileTrad.length()); //Controle
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();				
		}
	}
    
}
