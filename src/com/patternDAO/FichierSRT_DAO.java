package com.patternDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.beans.FichierSRT;
import com.beans.SousTitre;

/**
 * Objet DAO qui fait le lien avec la table fichier_sous_titre de la bdd
 * @author nicolas
 *
 */
public class FichierSRT_DAO extends DAOAbstract<FichierSRT>{

	/**
	 * Constructeur avec la connection vers la bdd
	 * @param pConn
	 */
	public FichierSRT_DAO(Connection pConn) {
		super(pConn);
		// TODO Auto-generated constructor stub
	}


	/**
	 * Methode d'insertion de donnees dans la table fichier_sous_titre
	 */
	@Override
	public boolean create(FichierSRT obj) {
		// TODO Auto-generated method stub
		
		try {
			this.conn.setAutoCommit(false);

			//--La requete pr�par�e
			PreparedStatement preparedStatement = this.conn.prepareStatement("INSERT INTO fichier_sous_titre (nom, langue_native, langue_traduction, avancement) VALUES(?, ?, ?, ?);");
			//--d�finition des parametres de la requetes
			preparedStatement.setString(1, obj.getNom());
			System.out.println(obj.getNom());//--Controle
		    preparedStatement.setString(2, obj.getLangueOrig());
		    System.out.println(obj.getLangueOrig());//--Controle
		    preparedStatement.setString(3, obj.getLangueTrad());
		    System.out.println(obj.getLangueTrad());//--Controle
		    preparedStatement.setInt(4, obj.getAvancement());
		    System.out.println(obj.getAvancement());//--Controle
		    
		    preparedStatement.executeUpdate();
						
			//--Ecriture de la requete et fermeture du flux
			preparedStatement.close();
			this.conn.commit();
			return true;			

		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * M�thode pour supprimer les donnees relatives au parametre dans la table fichier_sous_titre
	 * Inutile dans ce cas
	 */
	@Override
	public boolean delete(FichierSRT obj) {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * M�thode pour modifier les donnees relatives au parametre dans la table fichier_sous_titre
	 * Inutile dans ce cas
	 */
	@Override
	public boolean update(FichierSRT obj) {
		// TODO Auto-generated method stub
		return false;
	}


	/**
	 * M�thode pour trouver les donn�es de la table fichier_sous_titre
	 * @param id_fichier
	 * @return objet Fichier
	 */
	@Override
	public FichierSRT find(int id_fichier) {
		// TODO Auto-generated method stub
		FichierSRT fileSSTitre = new FichierSRT();
		try {
			this.conn.setAutoCommit(false);
			
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM fichier_sous_titre WHERE id_fichier = "+id_fichier+";");
			if (resultat.first()) {
				fileSSTitre.setId(id_fichier);
				fileSSTitre.setNom(resultat.getString("nom"));
				fileSSTitre.setLangueOrig(resultat.getString("langue_native"));
				fileSSTitre.setLangueTrad(resultat.getString("langue_traduction"));
				fileSSTitre.setAvancement(resultat.getInt("avancement"));
				
				DAOAbstract<SousTitre> ssTitreDAO = new SousTitre_DAO(MySQLConnection.getInstance());
				fileSSTitre.setSsTitre(ssTitreDAO.find(id_fichier));
			}

			resultat.close();
			this.conn.commit();
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return fileSSTitre;
	}

	/**
	 * M�thode pour trouver les donn�es de la table fichier_sous_titre
	 * @param nom
	 * @return objet Fichier
	 */
	@Override
	public FichierSRT find(String nom) {
		// TODO Auto-generated method stub
		FichierSRT fileSSTitre = new FichierSRT();
		try {
			this.conn.setAutoCommit(false);
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM fichier_sous_titre WHERE nom = ? ;");
			ps.setString(1, nom);
			ps.execute();
			
			if (ps.getResultSet().first()) {
				fileSSTitre.setId(ps.getResultSet().getInt("id_fichier"));
				fileSSTitre.setLangueOrig(ps.getResultSet().getString("langue_native"));
				fileSSTitre.setLangueTrad(ps.getResultSet().getString("langue_traduction"));
				fileSSTitre.setAvancement(ps.getResultSet().getInt("avancement"));
				
				DAOAbstract<SousTitre> ssTitreDAO = new SousTitre_DAO(MySQLConnection.getInstance());
				fileSSTitre.setSsTitre(ssTitreDAO.find(fileSSTitre.getId()));
			}

			ps.close();
			this.conn.commit();
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return fileSSTitre;
	}
	
	/**
	 * M�thode qui retourne une liste des nom des fichiers presents dans la bdd
	 * @return
	 */
	public ArrayList<String> listerFichier(){
		ArrayList<String> listFileBDD = new ArrayList<String>();
		try {
			this.conn.setAutoCommit(false);
			PreparedStatement ps = this.conn.prepareStatement("SELECT nom FROM fichier_sous_titre ;");
			ps.execute();
			
			while(ps.getResultSet().next()) {
				listFileBDD.add(ps.getResultSet().getString("nom"));
			}
			
			ps.close();
			this.conn.commit();
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("taille liste fichier "+listFileBDD.size());
		return listFileBDD;
		
	}


	@Override
	public FichierSRT getLigneDAO(String pLigne) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
