package com.patternDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.beans.SousTitre;





public class SousTitre_DAO extends DAOAbstract<SousTitre> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public SousTitre_DAO(Connection pConn) {
		super(pConn);
	}
	
	/**
	 * m�thode de cr�ation d'un objet sous titre dans la table sous titre
	 */
	
	@Override
	public boolean create(SousTitre obj) {
		// TODO Auto-generated method stub
		try {
			this.conn.setAutoCommit(false);
			
			//--La requete pr�par�e
			PreparedStatement preparedStatement = this.conn.prepareStatement("INSERT INTO sous_titre(ligne, original, traduction, id_fichier) VALUES(?, ?, ?, ?);");
			//--d�finition des parametres de la requetes
			for (int i = 0; i< obj.getSousTitreOrig().size(); i ++) {
					preparedStatement.setInt(1, i);
					//System.out.println(obj.getSousTitreOrig().get(i));//--Controle
		            preparedStatement.setString(2, obj.getSousTitreOrig().get(i));
		            //System.out.println(obj.getSousTitreTrad().get(i));//--Controle
		            preparedStatement.setString(3, obj.getSousTitreTrad().get(i));
		            //System.out.println(obj.getSousTitreTrad().get(i));//--Controle
		            preparedStatement.setInt(4, obj.getIdFichier());
		            System.out.println(obj.getIdFichier());//--Controle
		            preparedStatement.executeUpdate();
			}
			
			//--Ecriture de la requete et fermeture du flux
			preparedStatement.close();
			this.conn.commit();
			return true;			

		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(SousTitre obj) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * m�thode de modification d'un objet sous titre dans la table sous-titre
	 */
	@Override
	public boolean update(SousTitre obj) {
		// TODO Auto-generated method stub
		DAOAbstract<SousTitre> sstitreDAO = new SousTitre_DAO(MySQLConnection.getInstance());
		
		try {
			this.conn.setAutoCommit(false);
			
			//--La requete pr�par�e
			PreparedStatement preparedStatement = this.conn.prepareStatement("UPDATE sous_titre SET traduction = ? WHERE ligne = ? AND id_fichier = ? ;");
			PreparedStatement preparedStatementFichier = this.conn.prepareStatement("UPDATE fichier_sous_titre SET avancement = ? WHERE id_fichier = ? ;");
			System.out.println("ID_fich "+obj.getIdFichier());
			//System.out.println ("CTRL "+sstitreDAO.find(obj.getIdFichier()).getSousTitreTrad().size());//--Controle
			System.out.println ("CTRL+ "+obj.getSousTitreTrad().size());//--Controle

			int i = 0;
			while(i< obj.getSousTitreTrad().size())	{
				//System.out.println("requete : "+"UPDATE sous_titre SET traduction = '"+obj.getSousTitreTrad().get(i)+"' WHERE ligne = "+i);//--Controle
				//System.out.println("param "+i+ " "+sstitreDAO.find().getSousTitreTrad().get(i));//--Controle
								
				preparedStatement.setString(1, obj.getSousTitreTrad().get(i));
				preparedStatement.setInt(2, i);
				preparedStatement.setInt(3, obj.getIdFichier());
				preparedStatement.executeUpdate();

				i++;
			}
			
			//--MAJ de l'avancement
			int compteur = 0;
			for (String ligne : sstitreDAO.find(obj.getIdFichier()).getSousTitreTrad() ) {
				if (! ligne.isEmpty())
					compteur++;
			}
			preparedStatementFichier.setInt(1, ((compteur*100)/(sstitreDAO.find(obj.getIdFichier()).getSousTitreOrig().size())));
			preparedStatementFichier.setInt(2, obj.getIdFichier());
			preparedStatementFichier.executeUpdate();
			//--Ecriture de la requete et fermeture du flux
			preparedStatement.close();
			this.conn.commit();
			return true;
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	//@Override
	public SousTitre getLigneDAO(String pLigne) {
		// TODO Auto-generated method stub
		SousTitre sstitreLigne = new SousTitre();
		ArrayList<String> sstitreLigneOrig = new ArrayList<String>();
		ArrayList<String> sstitreLigneTrad = new ArrayList<String>();
		int id, ligneID = 0;
		//System.out.println(pLigne);
		try {
			this.conn.setAutoCommit(false);
			
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM sous_titre WHERE ligne = \""+pLigne+"\"");
			if(resultat.first()) {
				id = (Integer)resultat.getInt("id_fichier");
				//sstitreLigne.setId(id);
				ligneID = (Integer)resultat.getInt("ligne");
				sstitreLigneOrig.add((String)resultat.getString("original"));
				sstitreLigneTrad.add((String)resultat.getString("traduction"));
				sstitreLigne.setLigne(ligneID);
				sstitreLigne.setSousTitreOrig(sstitreLigneOrig);
				sstitreLigne.setSousTitreTrad(sstitreLigneTrad);
			}
			resultat.close();	
			this.conn.commit();
			
			
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return sstitreLigne;
	}	
	
	@Override
	public SousTitre find(int id_fichier) {
		// TODO Auto-generated method stub
        SousTitre sstitre = new SousTitre();
        ArrayList<String> sstitreorig = new ArrayList<String>();
        ArrayList<String> sstitretrad = new ArrayList<String>();
		try {
			this.conn.setAutoCommit(false);
			
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM sous_titre WHERE id_fichier = "+id_fichier+";");
				while(resultat.next()) {
					sstitreorig.add(resultat.getString("original"));
					sstitretrad.add(resultat.getString("traduction"));
				}
				sstitre.setSousTitreOrig(sstitreorig);
				sstitre.setSousTitreTrad(sstitretrad);

			resultat.close();
			this.conn.commit();
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return sstitre;
	}

	@Override
	public SousTitre find(String nom) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	@Override
	public ArrayList<String> listerFichier() {
		// TODO Auto-generated method stub
		return null;
	}



}
