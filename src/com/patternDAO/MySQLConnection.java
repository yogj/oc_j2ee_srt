package com.patternDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe permettant la connexion � la base de donnee MySQL
 * @author nicolas
 *
 */

public class MySQLConnection {
	
	// URL de connexion
		private String url = "jdbc:mysql://localhost:3306/activite_sous_titre?useUnicode=true&characterEncoding=utf-8";
		// Nom du user
		private String user = "yogj";
		// Mot de passe de l'utilisateur
		private String passwd = "ajx6tix";
		// Objet Connection
		private static Connection connect;
		private static MySQLConnection instance = new MySQLConnection();

		// Constructeur priv�
		private MySQLConnection() {
			  // Chargement du driver
	        try {
	            Class.forName("com.mysql.jdbc.Driver");
	        } catch (ClassNotFoundException e) {
	        	e.printStackTrace();
	        }
	        //connexion
			try {
				connect = DriverManager.getConnection(url, user, passwd);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// M�thode d'acc�s au singleton
		public static Connection getInstance() {
			if (connect == null)
				instance = new MySQLConnection();
			
			return connect;
		}
		/**
		 * M�thode pour fermer la connection
		 */
		public static void closeConnexion() {
			if(connect != null)
				try {
					connect.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
		}
}
