package com.patternDAO;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Classe Abstraite dont herite nos objetsDAO
 * @author nicolas
 *
 * @param <T>
 */
public abstract class DAOAbstract<T> {
	protected Connection conn = null;
	/**
	 * Constructeur avec co parametre la connexion etablie par le Connecteur
	 * @param pConn
	 */
	public DAOAbstract(Connection pConn){
		this.conn = pConn;
	}
	/**
	 * Methode abstraite de creation d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean create (T obj);
	/**
	 *  Methode abstraite de suppression d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean delete (T obj);
	/**
	 *  Methode abstraite de modification d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean update (T obj);
	/**
	 *  Methode abstraite de recherche d'un objet de type T � partir de son id
	 * @param id
	 * @return
	 */
	public abstract T find(int id);
	/**
	 * Methode abstraite de recherche d'un objet de type T � partir de son nom
	 * @param nom
	 * @return
	 */
	public abstract T find(String nom);
	
	/**
	 * M�thode abstraite pour recuperer la liste des fichiers de la table fichier_sous_titre
	 * @return
	 */
	public abstract ArrayList<String> listerFichier();

	/**
	 * M�thode pour recuperer le n� d'une ligne d'un objet T
	 * @param pLigne
	 * @return
	 */
	public abstract T getLigneDAO(String pLigne);

}
